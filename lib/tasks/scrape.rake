desc 'Scrapes pitchfork for unsaved reviews'
task :scrape_pitchfork, [:page_number, :rescrape] do |_, args|
	require 'pitchfork_review_parser'
	log = logger
	# Parse arguments
	page_number = (args[:page_number] || 1).to_i
	rescrape = !args[:rescrape].nil?
	done = false
	log.info('Starting scrape')
	until done && !rescrape
		log.info("Page #{page_number}")
		begin
			links = PitchforkReviewParser.page_links(page_number)
		rescue StandardError => e
				log.warn("Exception raised while parsing page #{page_number}")
				log.warn(e)
		end
		links.each do |link|
			log.info("Parsing #{link}")
			done = true unless Review.find_by(link: link).nil?
			begin
				PitchforkReviewParser.parse(link)
				sleep 1
			rescue StandardError => e
				log.warn("Exception raised while parsing #{link}")
				log.warn(e)
			end
		end
		page_number += 1
	end
	log.info('Scrape completed')
end

def logger
	log = Logger.new("#{Rails.root}/log/scraper.log")
	log.formatter = proc do |severity, datetime, _, msg|
		puts msg
		"#{severity} @ [#{datetime}]: #{msg} \n"
	end
	log
end
