# Module for parsing a single pitchfork review page
# The parse method takes a link as the only argument and
# saves all the data from the page into the database
module PitchforkReviewParser
	require File.expand_path('../config/environment', __dir__)
	def self.page_links(page_number)
		require 'open-uri'
		link = 'https://pitchfork.com/reviews/albums/?page=' << page_number.to_s
		doc = Nokogiri::HTML(open(link).read)
		doc.css('.review__link').map { |t| 'https://pitchfork.com' << t.attr('href') }
	end

	def self.parse(link)
		parser = PRPLogic.new(link)
		parser.parse
	end

	# Contains the logic of the parsing a review page
	class PRPLogic
		def initialize(link)
			@link = link
			require 'open-uri'
			@doc =  Nokogiri::HTML(open(link).read)
		end

		def parse
			genre
			reviewer
			review
			albums
			true
		end

			private

		def genre
			@genre = Genre.where(name: @doc.css('.genre-list__link').text).first_or_create
		end

		def review
			pub_date = Time.strptime(
			  @doc.css('.pub-date').attribute('datetime'),
			  '%Y-%m-%dT%H'
			)
			@review = Review.where(
			  abstract: @doc.css('.review-detail__abstract').text,
 			  link: @link,
 			  reviewer: @reviewer,
 			  content: @doc.css('.contents').text,
 			  published: pub_date
			).first_or_create
		end

		def reviewer
			name = @doc.css('.authors-detail__display-name').text
			@reviewer = Reviewer.where(name: name).first_or_create
		end

		def artists(tombstone, album)
			tombstone.css('.artist-list').css('li').each do |e|
				artist = Artist.where(name: e.text).first_or_create
				album.artists << artist unless album.artists.include? artist
			end
		end

		def albums
			@doc.css('.single-album-tombstone').each do |t|
				album(t)
			end
		end

		def album(tombstone)
			year = tombstone.css('.single-album-tombstone__meta-year').text
			a = Album.where(
			  title: tombstone.css('.single-album-tombstone__review-title').text,
 			  release: Time.new(year[2..6].to_i),
 			  genre: @genre
			).first_or_create
			labels(tombstone, a)
			artists(tombstone, a)
			score(tombstone, a)
			art(tombstone, a)
		end

		def art(tombstone, album)
			require 'open-uri'
			image_link = tombstone.css('.single-album-tombstone__art').css('img').attr('src')
			image = open(image_link)
			album.image.attach(io: image, filename: album.id)
		end

		def labels(tombstone, album)
			label_list = []
			labels = tombstone.css('.labels-list__item')
			labels.each do |l|
				label_list << Label.where(
				  name: l.text
				).first_or_create
			end
			assign_labels(album, label_list)
		end

		def assign_labels(album, labels)
			album.label = labels[0] if album.label.nil? && !labels[0].nil?
			album.reissue_label = labels[1] if album.reissue_label.nil? && !labels[1].nil?
			album.save
		end

		def score(tombstone, album)
			value = tombstone.css('.score').text.to_f
			s = Score.where(
			  album: album,
			  review: @review,
			  bnm: tombstone.css('.bnm-txt').text.include?('music'),
			  bnr: tombstone.css('.bnm-txt').text.include?('reissue')
			).first_or_create
			s.value = value
			s.save
		end
	end
end
