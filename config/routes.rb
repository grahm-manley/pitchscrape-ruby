# frozen_string_literal: true

Rails.application.routes.draw do
	get 'albums/:title', to: 'albums#show'
end
