require 'rails_helper'

RSpec.describe Genre, type: :model do
	it 'is valid with valid attributes' do
		expect(build(:genre)).to be_valid
	end
	it 'is unique' do
		g =	create(:genre)
		expect(Genre.new(name: g.name)).to_not be_valid
	end
end
