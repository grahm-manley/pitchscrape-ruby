require 'rails_helper'

RSpec.describe Artist, type: :model do
	it 'is valid with valid attributes' do
		expect(build(:artist)).to be_valid
	end
	it 'is not valid without a name' do
		expect(Artist.new).to_not be_valid
	end
	it 'is unique' do
		a = create(:artist)
		expect(Artist.new(name: a.name)).to_not be_valid
	end
	it 'can have many albums' do
		album1 = create(:album)
		album2 = create(:album)
		artist = create(:artist)
		artist.albums << album1
		artist.albums << album2
		expect(artist.albums.length).to be(2)
	end
end
