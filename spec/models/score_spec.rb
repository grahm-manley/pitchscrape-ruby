require 'rails_helper'

RSpec.describe Score, type: :model do
	it 'only accepts score of 1.0 to 10.0' do
		s = build(:score)
		s.value = 10.1
		expect(s).to_not be_valid
		s.value = -1
		expect(s).to_not be_valid
	end
end
