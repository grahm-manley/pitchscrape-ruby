require 'rails_helper'

RSpec.describe Album, type: :model do
	it 'is valid with valid attributes' do
		expect(build(:album)).to be_valid
	end
	it 'is not valid when missing title or release date' do
		expect(Album.new(title: 'title')).to_not be_valid
		expect(Album.new(release: Time.now)).to_not be_valid
	end
	it 'can have many artists' do
		expect(build(:album, artists_count: 3)).to be_valid
	end
end
