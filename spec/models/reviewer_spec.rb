require 'rails_helper'

RSpec.describe Reviewer, type: :model do
	it 'is valid with valid attributes' do
		expect(build(:reviewer)).to be_valid
	end
	it 'is unique' do
		r = create(:reviewer)
		expect(Reviewer.new(name: r.name)).to_not be_valid
	end
end
