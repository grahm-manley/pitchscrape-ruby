require 'rails_helper'

RSpec.describe Label, type: :model do
	it 'is valid with valid attributes' do
		expect(build(:label)).to be_valid
	end

	it 'is unique' do
		l = create(:label)
		expect(Label.new(name: l.name)).to_not be_valid
	end
end
