FactoryBot.define do
	factory :artist do
		sequence(:name) { |n| "artist#{n}" }
	end
	factory :album do
		sequence(:title) { |n| "title#{n}" }
		release Time.now
		label
		genre
		transient do
			artists_count { 1 }
		end
		after(:create) do |album, evaluator|
			create_list(:artist, evaluator.artists_count, albums: [album])
		end
	end
	factory :label do
		sequence(:name) { |n| "label#{n}" }
	end
	factory :genre do
		sequence(:name) { |n| "genre#{n}" }
	end
	factory :reviewer do
		sequence(:name) { |n| "reviewer#{n}" }
	end
	factory :review do
		published Time.now
		reviewer
	end
	factory :score do
		album
		review
		value 5
	end
end
