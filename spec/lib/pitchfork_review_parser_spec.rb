require 'rails_helper'
require 'pitchfork_review_parser'

RSpec.describe PitchforkReviewParser do
	it 'parses review from page' do
		PitchforkReviewParser.parse('https://pitchfork.com/reviews/albums/22420-22-a-million/')
		r = Review.last
		expect(r.link).to eq('https://pitchfork.com/reviews/albums/22420-22-a-million/')
		expect(r.reviewer).to eq(Reviewer.find_by(name: 'Amanda Petrusich'))
	end

	it 'parses album from page' do
		PitchforkReviewParser.parse('https://pitchfork.com/reviews/albums/22420-22-a-million/')
	end

	it 'parses multiple album review page' do
		PitchforkReviewParser.parse('https://pitchfork.com/reviews/albums/22295-blonde-endless/')
		expect(Review.last.link).to eq('https://pitchfork.com/reviews/albums/22295-blonde-endless/')
	end

	it 'parses single album with multiple artists' do
		PitchforkReviewParser.parse('https://pitchfork.com/reviews/albums/courtney-barnett-kurt-vile-lotta-sea-lice/')
	end

	it 'parses album with multiple labels' do
		PitchforkReviewParser.parse('https://pitchfork.com/reviews/albums/bob-dylan-more-blood-more-tracks-the-bootleg-series-vol-14/')
	end
end
