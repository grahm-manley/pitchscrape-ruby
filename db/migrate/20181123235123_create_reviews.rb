# Create reviews table
class CreateReviews < ActiveRecord::Migration[5.2]
  def change
    create_table :reviews do |t|
      t.references :reviewer, foreign_key: true
      t.references :album, foreign_key: true
      t.decimal :score
      t.date :published

      t.timestamps
    end
  end
end
