# Add abstract column to reviews table
class AddAbstractToReviews < ActiveRecord::Migration[5.2]
  def change
    add_column :reviews, :abstract, :text
  end
end
