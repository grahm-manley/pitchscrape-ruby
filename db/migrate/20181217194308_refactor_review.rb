# Refactoring so that review can have multiple albums
# Added content and link columns
class RefactorReview < ActiveRecord::Migration[5.2]
  def change
	remove_column :reviews, :album_id if column_exists? :review, :album_id
	remove_column :reviews, :score

	add_column :reviews, :content, :text
	add_column :reviews, :link, :string
  end
end
