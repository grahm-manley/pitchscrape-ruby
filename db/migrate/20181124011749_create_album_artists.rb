# Create many to many relationship between albums and artists
class CreateAlbumArtists < ActiveRecord::Migration[5.2]
  def change
    create_table :albums_artists, id: false do |t|
      t.belongs_to :album, index: true
      t.belongs_to :artist, index: true
    end
  end
end
