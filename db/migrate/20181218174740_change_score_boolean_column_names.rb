# Make BNM and BNR lowercase
class ChangeScoreBooleanColumnNames < ActiveRecord::Migration[5.2]
  def change
	rename_column :scores, :BNM, :bnm
	rename_column :scores, :BNR, :bnr
  end
end
