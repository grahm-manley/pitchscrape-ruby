# Create albums table
class CreateAlbums < ActiveRecord::Migration[5.2]
  def change
    create_table :albums do |t|
      t.string :title
      t.date :release
      t.references :label, foreign_key: true
      t.references :genre, foreign_key: true
      t.references :reissue_label, foreign_key: { to_table: :labels }
      t.timestamps
    end
  end
end
