# Create reviewers table
class CreateReviewers < ActiveRecord::Migration[5.2]
  def change
    create_table :reviewers do |t|
      t.string :name

      t.timestamps
    end
  end
end
