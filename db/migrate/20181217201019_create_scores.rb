# Scores created for many to many relationship between albums and reviews
# Also includes columns for 'Best New Music' and 'Best New Reissue'
class CreateScores < ActiveRecord::Migration[5.2]
  def change
    create_table :scores do |t|
		t.belongs_to :album, index: true
		t.belongs_to :review, index: true
		t.decimal :value
		t.boolean :BNM
		t.boolean :BNR
    end
  end
end
