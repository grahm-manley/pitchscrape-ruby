# Scale was previously not specified and defaulted to 0 which is quite
# problematic
class ChangeScoreValueScale < ActiveRecord::Migration[5.2]
  def change
	change_column :scores, :value, :decimal, precision: 3, scale: 1	
  end
end
