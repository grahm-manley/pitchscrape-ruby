# Pitchscrape
Pitchscrape is a backend service built in Ruby on Rails that scrapes
Pitchfork.com periodically to keep an updated database of all the site's
reviews. It responds with JSON objects. The production version
is [here](https://www.pitchscrape.grahmmanley.com) (though the front page is blank).
## Set up

### rbenv
[rbenv](https://github.com/rbenv/rbenv) is used to set the ruby version.
Use ruby version 2.5.3 

### Install Dependencies  
`bundle`

### Set up the database
`rake db:create`

### Start the server
`rails s`

### Run a Pitchfork scrape
The following will run a scrape from page 1 and will continue until it finds
a review that already exists in the database. 

`rake scrape_pitchfork`

To start at a certain page use a page number as an argument like so

`rake scrape_pitchfork[5]`

To have the scraper continue even after an existing review is found use the following

`rake scrape_pitchfork[1,y]`

Log files for scrapes are saved at `log/scraper.log`

Note: zsh isn't compatible with bracketed arguments so you might have to 
use something like `rake scrape_pitchfork\[1\,y\]`

### Accessing the API
Album requests are formatted like so 
`baseurl.com/albums/some album/?artist[]=artist one&artist[]=artist two`. 
The API will respond with a json representation of the pitchfork review if
it exists, and a 404 'not found' code if not.

## Development Standards
* Use rubocop to lint your code
* Use test driven development
* In git use rebase instead of merge in order to keep the history clean

## Development To Do list
* Get to 100% code coverage
* Deal with rails command warnings
* Make a presentable front page with pitchfork stats
