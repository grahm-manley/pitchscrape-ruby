# Controller for the album resource
class AlbumsController < ApplicationController
	def show
		albums = Album.where(title: params[:title])
		return not_found if albums.empty?

		return render json: albums.first if params[:artists].nil?

		render json: find_artist_match(albums)
	end

		private

	def find_artist_match(albums)
		artists = []
		albums.each do |album|
			album.artists.each do |artist|
				artists << artist.name
			end
			return album if artists & params[:artists] == artists
		end
		albums.first
	end

	def not_found
		render json: { message: 'Not found', code: '404' }, status: :not_found
	end
end
