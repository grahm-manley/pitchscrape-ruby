# This decides the format that the album model is serialized
class AlbumSerializer < ActiveModel::Serializer
	include Rails.application.routes.url_helpers
	attributes :id, :title, :artists, :scores, :reviews, :image_url

	def image_url
		rails_blob_url(object.image)	
	end
end
