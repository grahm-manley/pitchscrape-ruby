# Review model
class Review < ApplicationRecord
	belongs_to :reviewer
	has_many :scores
	has_many :albums, through: :scores
end
