# Album Model
class Album < ApplicationRecord
	belongs_to :label, required: false
	belongs_to :reissue_label, class_name: :Label, required: false
	belongs_to :genre, required: false
	has_and_belongs_to_many :artists
	has_many :scores
	has_many :reviews, through: :scores
	has_one_attached :image
	validates_presence_of :title
	validates_presence_of :release
end
