# Reviewer Model
class Reviewer < ApplicationRecord
	validates :name, presence: true, uniqueness: true
end
