# Score model takes care of many to many relationship
# between album and reviews and contains the score value
# as well as booleans values representing whether an album in a
# 'Best New Reissue' or 'Best New Album'
class Score < ApplicationRecord
	validates :value, numericality: { greater_than: 0, less_than: 10 }

	belongs_to :album
	belongs_to :review
end
